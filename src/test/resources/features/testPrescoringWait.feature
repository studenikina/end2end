#language: ru

Функционал: Проверка заполнения страницы "Ожидание прескоринга"

    @CheckPrescoringWait
    Сценарий: Проверка отображения страницы "Ожидание прескоринга".
    И открывается страница "Загрузка"
    И поле "Ожидание скоринга" заполнено значением "до предварительного решения осталось"
    И поле "Время ожидания" заполнено значением "3-х минут"