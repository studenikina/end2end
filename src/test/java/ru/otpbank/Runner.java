package ru.otpbank;

import cucumber.api.junit.Cucumber;
import io.cucumber.core.options.FeatureOptions;
import org.junit.runners.model.InitializationError;
import ru.otpbank.testAnketa.util.FeatureProcessor;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class Runner extends Cucumber {

    static {
        FeatureProcessor.proceed();
    }

    public Runner(Class clazz) throws InitializationError {
        super(clazz);
    }
}
