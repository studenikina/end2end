package ru.otpbank.util;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.otpbank.testAnketa.settings.DriverManager;
import ru.otpbank.testAnketa.settings.TestProperties;

import java.util.Properties;
import java.util.concurrent.TimeUnit;


/**
 * Класс реализует методы открытия и закрытия сессии веб-браузера
 */
public class AbstractTest {

    public static Properties properties = TestProperties.getInstance().getProperties();
    private static WebDriver driver = DriverManager.getDriver();

    @Before
    public void openBrowser() {
        driver.get(properties.getProperty("app.url"));
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        new WebDriverWait(driver, 20).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("form"));
    }

    @After
    public void closeBrowser() { DriverManager.quiteDriver(); }
}
