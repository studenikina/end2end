package ru.otpbank.util;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import io.cucumber.core.options.FeatureOptions;
import org.junit.runner.RunWith;
import ru.otpbank.Runner;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@RunWith(Runner.class)
@CucumberOptions(
        features = "classpath:features/",
        glue = {"ru.otpbank.testAnketa.steps","ru.otpbank.testAnketa.hooks"},
        tags = {"@ZayasSuperPuperTesting"},
        plugin = "io.qameta.allure.cucumber4jvm.AllureCucumber4Jvm",
        snippets = SnippetType.CAMELCASE
)
public class TestSupport {
}
