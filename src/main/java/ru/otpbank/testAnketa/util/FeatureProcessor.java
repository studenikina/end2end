package ru.otpbank.testAnketa.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.otpbank.testAnketa.settings.PageFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FeatureProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(PageFactory.class);
    private static final List<Path> FEATURES = new ArrayList<>();
    private static final Map<String, Scenario> SCENARIOS = new HashMap<>();
    private static final Pattern IMPORT_PATTERN = Pattern.compile("(?<=Подключить:).*$");
    private static final Pattern SCENARIO_NAME_PATTERN = Pattern.compile("(?<=Сценарий:).*$");

    public static void proceed() {
        Path target;

        try {
            target = Paths.get(Objects.requireNonNull(FeatureProcessor.class.getClassLoader().getResource("features/")).toURI());
        } catch (URISyntaxException e) {
            LOG.error("Что-то пошло не так при попытке получить путь до директории с feature файлами." + e);
            throw new RuntimeException("Что-то пошло не так при поиске директории с feature файлами" + e);
        }

        try {
            FEATURES.addAll(
                    Files.find(
                            target,
                            Integer.MAX_VALUE,
                            (path, basicFileAttr) ->
                                    path.toString().endsWith(".feature") && basicFileAttr.isRegularFile()
                    ).collect(Collectors.toList())
            );
            for (Path feature : FEATURES) {
                parseScenarios(feature);
            }
            for (Path feature : FEATURES) {
                process(feature);
            }
        } catch (IOException e) {
            LOG.error("Что-то пошло не так при попытке получить конкретные feature файлы" + e);
            throw new RuntimeException("Что-то пошло не так при попытке получить конкретные feature файлы" + e);
        }
    }

    private static void parseScenarios(Path feature) throws IOException {
        List<String> lines = Files.readAllLines(feature).stream()
                .filter(s -> !s.isEmpty() && !s.startsWith("#"))
                .map(String::trim)
                .collect(Collectors.toList());

        Map<String, List> scenarios = new HashMap<>();

        String scnName = null;
        List scnSteps = new ArrayList();
        boolean inScnBody = false;
        for (int i = 0; i < lines.size(); i++) { // TODO: Переделать весь блок
            String line = lines.get(i);

            if (!inScnBody && line.startsWith("Сценарий:")) {
                Matcher matcher = SCENARIO_NAME_PATTERN.matcher(line.trim());
                if (matcher.find()) {
                    scnName = matcher.group(0).trim();
                    inScnBody = true;
                } else {
                    throw new RuntimeException("Что-то пошло не так при попытке получить имя сценария. " + line);
                }
            } else if (inScnBody) {
                scnSteps.add(line);
                if (i != lines.size() - 1) {
                    String nextLine = lines.get(i + 1);
                    if (nextLine.startsWith("Сценарий:") || nextLine.startsWith("@")) {
                        scenarios.put(scnName, new ArrayList(scnSteps));
                        scnName = null;
                        scnSteps.clear();
                        inScnBody = false;
                    }
                } else {
                    scenarios.put(scnName, scnSteps);
                }
            }
        }

        for (Map.Entry<String, List> scenario : scenarios.entrySet()) {
            Scenario scn = new Scenario(scenario.getKey(), scenario.getValue());
            SCENARIOS.put(scenario.getKey(), scn);
        }
    }

    private static void process(Path feature) throws IOException {
        List<String> lines = Files.readAllLines(feature).stream()
                .filter(s -> !s.isEmpty())
                .map(String::trim)
                .collect(Collectors.toList());

        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);

            Matcher matcher = IMPORT_PATTERN.matcher(line.trim());
            if (matcher.find()) {
                String scenarioName = matcher.group(0);
                lines.remove(i);
                lines.addAll(i, SCENARIOS.get(scenarioName.trim()).getSteps());

                Files.write(feature, lines, StandardOpenOption.WRITE);
            }
        }
    }

    private static class Scenario {
        private String name;
        private List steps;

        Scenario(String name, List steps) {
            this.name = name;
            this.steps = new ArrayList(steps);
        }

        String getName() { return name; }
        List getSteps() { return steps; }

        @Override
        public String toString() {
            return "Scenario[" +
                    "name=" + name +
                    ", steps count=" + steps.size()
                    + "]";
        }
    }
}