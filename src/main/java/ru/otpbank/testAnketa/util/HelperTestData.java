package ru.otpbank.testAnketa.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import ru.otpbank.testAnketa.page.AbstractPageObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Random;


public class HelperTestData extends AbstractPageObject{

    public Map<String, String> getTestData(String testData, Class clazz) throws IOException {
        ObjectMapper objMapper = new ObjectMapper();
        try {
            return objMapper.readValue(
                    clazz.getResource(testData + ".json"),
                    new TypeReference<Map<String, String>>(){}
            );
        } catch (JsonParseException e) {
            Assert.fail("Неправильный json file: " + testData+ " " + e);
        } catch (JsonMappingException e) {
            Assert.fail("Invalid Json object. Expected array of pair key/value.");
        } catch (IOException e) {
            Assert.fail("Something went wrong while try to open the file with test data. FileName: " + testData + " " + e);
        } return null;
    }

    private static String namesList [] = {"Иван", "Сергей", "Петр", "Олег", "Леонид", "Тимофей", "Александр"};
    /**
     * Генератор случайный чисел в указанном диапазоне
     * @param from
     * @param to
     * @return
     */
    public static int getRandomNumberRange(int from, int to) {
        Random rnd = new Random(System.currentTimeMillis());
        return  (from + rnd.nextInt(to - from + 1));}

    public static String getRandomPassport (){ return "0000" + String.valueOf(getRandomNumberRange(100000,999999)); }

    /**
     * Генератор случайного имени
     * @return
     */
    public static String getRandomName() {
        ArrayList<String> randomName = new ArrayList<String>();
        Collections.addAll(
                randomName,
                "Иван", "Герман", "Петр", "Олег", "Леонид", "Лоренс", "Александр");

        return randomName.get(new Random().nextInt(randomName.size()));
    }

    /**
     * Генератор случайного отчества
     * @return
     */
    public static String getRandomMiddleName() {
        return getRandomName() + "ович";
    }

    /**
     * Генератор случаного номера телефона
     */
    public static String getRandomPhone () {
        return  String.valueOf(getRandomNumberRange(901, 999) + "-" + getRandomNumberRange(100 ,999) + "-" +
                getRandomNumberRange(78,99) + "-" + getRandomNumberRange(11,97));
    }
}

