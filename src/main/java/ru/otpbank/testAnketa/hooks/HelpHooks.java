package ru.otpbank.testAnketa.hooks;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.otpbank.testAnketa.settings.DriverManager;
import ru.otpbank.testAnketa.settings.TestProperties;
import ru.otpbank.testAnketa.steps.PageSteps;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class HelpHooks {
    private static Properties properties = TestProperties.getInstance().getProperties();
    private static final Logger LOG = LoggerFactory.getLogger(PageSteps.class);

    @Before
    public void openBrowser(){
        WebDriver driver = DriverManager.getDriver();
        driver.get(properties.getProperty("app.url"));
        driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
        new WebDriverWait(DriverManager.getDriver(), 20).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("form"));
    }

//    @After
//    public void afterMethod(Scenario scenario) {
//        afterScenario(scenario);
//        DriverManager.quiteDriver();
//    }


    private static void afterScenario(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                byte[] screenshot = ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
