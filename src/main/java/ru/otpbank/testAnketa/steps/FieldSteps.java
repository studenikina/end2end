package ru.otpbank.testAnketa.steps;

import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Тогда;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.otpbank.testAnketa.settings.PageFactory;
import ru.otpbank.testAnketa.util.HelperTestData;
import java.util.Map;


public class FieldSteps {

    private static final Logger LOG = LoggerFactory.getLogger(FieldSteps.class);
    HelperTestData helperTestData = new HelperTestData();

    @И("^поле \"([^\"]*)\" заполняется значением \"([^\"]*)\"$")
    public void fillingField(String nameField, String value) {
        PageFactory.getPageFactory().getCurrentPage().fillField(nameField, value);
        LOG.debug("Поле {} заполнено значением {}", nameField, value);
    }

    @И("^поле \"([^\"]*)\" заполняется случайным значением")
    public void fillingFieldRandomValue (String nameField) {
        if (nameField.equals("Серия и номер паспорта")) {
            String randomPassport =  helperTestData.getRandomPassport();
            PageFactory.getPageFactory().getCurrentPage().fillField(nameField,randomPassport);
            LOG.debug("Поле {} заполнено значением {}", nameField,randomPassport);
        } else if (nameField.equals("Мобильный телефон")||nameField.equals("Телефон по месту регистрации")){
            String randomPhone = helperTestData.getRandomPhone();
            PageFactory.getPageFactory().getCurrentPage().fillField(nameField,randomPhone);
            LOG.debug("Поле {} заполнено значением {}", nameField,randomPhone);
        }
    }

    @И("очищается поле \"([^\"]*)\"$")
    public void clearField (String field) {
        PageFactory.getPageFactory().getCurrentPage().getField(field).click();
        PageFactory.getPageFactory().getCurrentPage().getField(field).clear();
    }

    @И("поля заполняются значениями:")
    public void fillingFields(Map <String, String> fields) {
        fields.forEach((k,v) -> fillingField(k.toString(), v.toString()));
    }

    @И("^в выпадающем списке \"([^\"]*)\" выбирается значение \"([^\"]*)\"$")
    public void setValueFromDropDown(String nameList, String value) {
        //fillingField(nameList,value);
        PageFactory.getPageFactory().getCurrentPage().choiseValueElem(nameList, value);
        LOG.debug("Поле {} заполнено значением {}", nameList, value);
    }



    @И("^поле \"([^\"]*)\" заполнено значением \"([^\"]*)\"$")
    public void getTextValue(String fieldName, String value) {
        try {
                String valueFromPage = PageFactory.getPageFactory().getCurrentPage().getElementValue(fieldName);
            if (valueFromPage.contains(value)) {
                LOG.debug("Поле {} заполнено значением {}", fieldName,value);
            } else {
                Assert.fail("Проверка содержимого поля не пройдена!\n" +
                        "Ожидаемый результат: для поля: "+ fieldName +" должно быть установлено значение: " +  value + "\n" +
                        "Фактический результат: для поля: "+ fieldName + " установлено значение \n" + valueFromPage);
                }
        } catch (NullPointerException e){
            String valueFromElement = PageFactory.getPageFactory().getCurrentPage().getTextValueFromValue(fieldName);
            if (valueFromElement.contains(value)) {
                LOG.debug("Поле {} заполнено значением {}", fieldName,value);
            } else {
                Assert.fail("Проверка содержимого поля не пройдена!\n" +
                        "Ожидаемый результат: для поля: "+ fieldName +" должно быть установлено значение: " +  value + "\n" +
                        "Фактический результат: для поля: "+ fieldName + " установлено значение \n" + valueFromElement);
            }
        }

    }


    @И("^поле \"([^\"]*)\" не пустое")
    public void getTextValue(String nameField) {
        String valueFromPage = PageFactory.getPageFactory().getCurrentPage().getElementValue(nameField);
        if(!valueFromPage.equals("")){
            LOG.debug("Проверка наличия содержимого пройдена - Для поля {} установлено значение {}", nameField, valueFromPage);
        } else {
            Assert.fail("Проверка содержимого поля не пройдена!\n" +
                    "Ожидаемый результат: поле: "+ nameField +" должно быть пустое \n" +
                    "Фактический результат: для поля: "+ nameField + " установлено значение \n" + valueFromPage);
        }
    }

    @Тогда("поля заполнены значениями:")
    public void getValueFromFields(Map<String, String> fields) {
        fields.forEach((k, v) -> getTextValue(k.toString(), v.toString()));
    }

    @Тогда("в выпадающем списке \"([^\"]*)\" установлено значение \"([^\"]*)\"$")
    public void getValueFromList(String nameList, String value) {
        String valueFromPage = PageFactory.getPageFactory().getCurrentPage().getField(nameList).getText();
        if (valueFromPage.equals(value)) {
            LOG.debug("Проверка содержимого поля пройдена - Для поля {} установлено значение {}", nameList, value);
        } else {
            Assert.fail("Проверка содержимого поля не пройдена!\n" +
                    "Ожидаемый результат: для поля: "+ nameList +" должно быть установлено значение: " +  value + "\n" +
                    "Фактический результат: для поля: "+ nameList + " установлено значение \n" + valueFromPage);
        }
    }

    @И("Пользователь видит для поля \"([^\"]*)\" текст предупреждения \"([^\"]*)\"$")
    public void getErrorText(String field, String errorText) {
        String errorTextField = PageFactory.getPageFactory().getCurrentPage().getErrorFromField(field);
        if (errorTextField.equals(errorText)) {
            LOG.debug("Проверка содержимого поля пройдена - Для поля {} установлено значение {}", field, errorText);
        } else {
            Assert.fail("Проверка содержимого поля не пройдена!\n" +
                    "Ожидаемый результат: для поля: "+ field +" должно быть установлено значение: " +  errorText + "\n" +
                    "Фактический результат: для поля: "+ field + " установлено значение \n" + errorText);
        }
    }

    @И("пользователь видит для полей текст предупреждения:")
    public void getErrorMessage(Map<String, String> errorTable) {
        errorTable.forEach((k, v) -> getErrorText(k.toString(), v.toString()));
    }

    @И("Пользователь видит для поля \"([^\"]*)\" текст  \"([^\"]*)\"$")
    public void getTextFrom(String field, String errorText) {
        String errorTextField = PageFactory.getPageFactory().getCurrentPage().getElementText(field);
        if (errorTextField.equals(errorText)) {
            LOG.debug("Проверка содержимого поля пройдена - Для поля {} установлено значение {}", field, errorText);
        } else {
            Assert.fail("Проверка содержимого поля не пройдена!\n" +
                    "Ожидаемый результат: для поля: "+ field +" должно быть установлено значение: " +  errorText + "\n" +
                    "Фактический результат: для поля: "+ field + " установлено значение \n" + errorText);
        }
    }


    @И("нажимается кнопка \"([^\"]*)\"$")
    public void clickElement(String nameElement) {
        PageFactory.getPageFactory().getCurrentPage().clickElemet(nameElement);
        LOG.debug("Выполнено нажатие на кнопку:" + nameElement);
    }


}
