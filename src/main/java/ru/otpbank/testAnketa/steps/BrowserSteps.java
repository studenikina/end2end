package ru.otpbank.testAnketa.steps;

import cucumber.api.java.ru.Пусть;
import org.openqa.selenium.WebDriver;
import ru.otpbank.testAnketa.settings.DriverManager;
import ru.otpbank.testAnketa.settings.TestProperties;

import java.util.Properties;

public class BrowserSteps {
    private static Properties properties = TestProperties.getInstance().getProperties(); // TODO: Delete
    WebDriver driver = DriverManager.getDriver();

    @Пусть("^пользователь открывает браузер$")
    public void openBrowser() {
        DriverManager.getDriver();
    }

    @Пусть("^пользователь переходит по ссылке$")
    public void goToByUrl() {
        DriverManager.getDriver().get(properties.getProperty("app.url")); // TODO: Delete hardcode
    }

}
