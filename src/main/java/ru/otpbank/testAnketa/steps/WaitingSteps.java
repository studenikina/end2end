package ru.otpbank.testAnketa.steps;

import cucumber.api.java.ru.Пусть;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.otpbank.testAnketa.settings.DriverManager;

import java.util.concurrent.TimeUnit;

public class WaitingSteps {
    private static final Logger LOG = LoggerFactory.getLogger(WaitingSteps.class);

    @Пусть("^пользователь ожидает загрузки страницы в течении некоторого времени")
    public void waitLoad() {
        long start = System.currentTimeMillis();
        DriverManager.getDriver().manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
        long timeLoadPage = System.currentTimeMillis() - start;
        LOG.debug("Ожидание загрузки страницы в секундах: " + timeLoadPage);
    }
}
