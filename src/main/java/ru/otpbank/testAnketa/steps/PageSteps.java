package ru.otpbank.testAnketa.steps;

import cucumber.api.java.ru.И;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.otpbank.testAnketa.annotation.PageName;
import ru.otpbank.testAnketa.settings.DriverManager;
import ru.otpbank.testAnketa.settings.PageFactory;

public class PageSteps {
    private static final Logger LOG = LoggerFactory.getLogger(PageSteps.class);

    @И("^открывается страница \"([^\"]*)\"$")
    public void checkIsPageOpened(String name) {
        long start = System.currentTimeMillis();

        PageName pageNameAnnotation = PageFactory.getPageFactory().getPage(name).getClass().getAnnotation(PageName.class);
        if (name.equals("Загрузка")){
            new WebDriverWait(DriverManager.getDriver(), 300).until(ExpectedConditions.
                    visibilityOfElementLocated(By.xpath(pageNameAnnotation.path())));
        } else
            new WebDriverWait(DriverManager.getDriver(), 300).until(
                ExpectedConditions.textToBePresentInElementLocated(
                        By.xpath(pageNameAnnotation.path()),
                        pageNameAnnotation.name()
                )
        );

        DriverManager.getDriver().switchTo().defaultContent();
        ((JavascriptExecutor) DriverManager.getDriver()).executeScript("arguments[0].scrollIntoView(true);",
                DriverManager.getDriver().findElement(By.cssSelector("[class = 'form']")));
         DriverManager.getDriver().switchTo().frame("form");

        long timeLoadPage = System.currentTimeMillis() - start;
        LOG.debug("Открылась страница: " + name + " в течении: " + timeLoadPage + " милисекунд");
    }
}
