package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.PageName;

/**
 * Page Object, описывающий форму "По УА требуется верификация"
 */
@PageName(name = "По УА требуется верификация", path = "")
public class ScoringVerification extends AbstractPageObject {

    @FindBy(xpath = ".//div[@class = 'result']/descendant::h2[1]")
    WebElement scorVerAU;
}
