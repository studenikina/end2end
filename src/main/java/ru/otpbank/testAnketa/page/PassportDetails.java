package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.annotation.PageName;

/**
 * Page Object, описывающий форму "Паспортные данные"
 */
@PageName(name = "Паспортные данные", path = ".//h3[text()= 'Паспортные данные']")
public class PassportDetails extends AbstractPageObject {

    @FindBy(css = "[name = 'passport']")
    @FieldName(name = "Серия и номер паспорта")
    public WebElement seriesAndPassportNumber;

    @FindBy(css = "[name = 'passport_date']")
    @FieldName(name = "Дата выдачи")
    public WebElement dateOfIssue;

    @FindBy(css = "[name = 'passport_unit_code']")
    @FieldName(name = "Код подразделения")
    public WebElement divisionCode;

    @FindBy(css = "[name = 'passport_issued_by']")
    @FieldName(name = "Кем выдан (как в паспорте)")
    public WebElement placeOfIssue;

    @FindBy(css = "[name = 'birth_settlement']")
    @FieldName(name = "Место рождения (как в паспорте)")
    public WebElement placeOfBirth;

    @FindBy(css = "[name = 'registry_address']")
    @FieldName(name = "Адрес постоянной регистрации")
    public WebElement permanentRegistrationAddress;

    @FindBy(xpath = ".//span[text() = 'Мужской']")
    @FieldName(name = "Мужской пол")
    public WebElement sexM;

    @FindBy(xpath = ".//span[text() = 'Женский']")
    @FieldName(name = "Женский пол")
    public WebElement sexW;

    @FindBy(xpath = ".//span[text() = 'У меня есть старый паспорт']")
    @FieldName(name = "Есть старый паспорт")
    public WebElement usedPassport;

    @FindBy(xpath = ".//label[text()= 'серия и номер старого паспорта']/ancestor::div[2]/descendant::div[2]")
    @FieldName(name = "Серия и номер старого паспорта")
    public WebElement oldPassportDetails;

    @FindBy(xpath = ".//span[text()='Узнать предварительное решение']")
    @FieldName(name = "Узнать предварительное решение")
    public WebElement preliminaryDecision;
}
