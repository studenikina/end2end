package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.annotation.PageName;

import java.io.File;

/**
 * Page Object, описывающий форму "Решение прескоринга"
 */
@PageName(name = "предварительно одобрена", path = ".//span[text() = 'предварительно одобрена']")
public class PrescoringDecision extends AbstractPageObject {


    @FindBy(xpath = ".//h2[@class = 'heading heading_size_2 heading_center result__heading']")
    @FieldName(name = "Отрицательное решение")
    public WebElement RejDecisionPrescoring;

    @FindBy(xpath = ".//span[text() = 'Продолжить']")
    @FieldName(name = "Продолжить")
    public WebElement buttonContinue;


    @FindBy (xpath = ".//h4[contains(text(), 'Заполните')]")
    @FieldName(name = "Мотивация")
    public WebElement textDecision;
}
