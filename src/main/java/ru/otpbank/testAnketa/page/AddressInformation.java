package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.annotation.PageName;

/**
 * Page Object, описывающий форму "Адрес"
 */
@PageName(name = "Адрес", path = ".//h3[text()= 'Адрес']")
public class AddressInformation extends AbstractPageObject {

    @FindBy(xpath = ".//*[@name = 'registry_address' and(@class ='input input_combobox')]")
    @FieldName(name = "Адрес постоянной регистрации")
    public WebElement permanentRegistrationAddress;

    @FindBy(css = "[name = 'registry_address_registry_date']")
    @FieldName(name = "Дата регистрации")
    public WebElement registrationsDate;

    @FindBy(css = "[name = 'registry_phone']")
    @FieldName(name = "Телефон по месту регистрации")
    public WebElement phoneOfRegistration;

    @FindBy(xpath = ".//span[text() = 'Совпадает с адресом фактич. проживания']")
    @FieldName(name = "Совпадает с адресом фактического проживания")
    public WebElement checkboxActualAddress;

    @FindBy(xpath = ".//span[text() = 'Совпадает с почтовым адресом']")
    @FieldName(name = "Совпадает с почтовым адресом")
    public WebElement checkboxPost_address;

    @FindBy(css = "[name = 'fact_address']")
    @FieldName(name = "Фактический адрес")
    public WebElement actualAddress;

    @FindBy(css = "[name = 'post_address']")
    @FieldName(name = "Почтовый адрес")
    public WebElement postAdress;

    @FindBy(css = "[name = 'fact_phone']")
    @FieldName(name = "Телефон по месту фактического проживания")
    public WebElement postPhone;

    @FindBy(xpath = ".//span[text() = 'Далее']")
    @FieldName(name = "Далее")
    public WebElement buttonNext;
}
