package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.annotation.PageName;

/**
 * Page Object, описывающий форму "Решение основного скоринга"
 */
@PageName(name = "Мы рады сообщить, что запрошенная вами сумма одобрена.", path = ".//h6[@class='heading heading_size_6 heading_center form-success__heading-description']")
public class ScoringDecision extends AbstractPageObject {

    @FindBy(xpath = ".//h2[@class= 'heading heading_size_2 heading_center']")
    @FieldName(name = "Решение скоринга")
    public WebElement DecisionScoring;

    @FindBy(xpath = ".//h6[@class='heading heading_size_6 heading_center form-success__heading-description']")
    @FieldName(name = "Информация одобрения")
    public WebElement approvalInformation;

    @FindBy(xpath= ".//div[text() = 'Срок кредита']/ancestor::div[1]/descendant::div[4]")
    @FieldName(name = "Срок кредита")
    public WebElement loanTerm;

    @FindBy(xpath = ".//div[text() = 'Ежемесячный платеж']/ancestor::div[1]/descendant::div[4]" )
    @FieldName(name = "Ежемесячный платеж")
    public WebElement monthlyPayment;

    @FindBy(xpath = ".//a[text() = 'Страхование жизни']/following-sibling::span[@class= 'form-success-insurance-item__price']")
    @FieldName(name = "Страхование жизни")
    public WebElement insuranceLife;

    @FindBy(xpath = ".//a[text() = 'Страхование от потери работы']/following-sibling::span[@class= 'form-success-insurance-item__price']")
    @FieldName(name = "Страхование от потери работы")
    public WebElement insuranceWork;

    @FindBy(xpath = ".//div[@class='alert__content']")
    @FieldName(name = "Информация о страховке")
    public WebElement warningInsurance;

    @FindBy(xpath = ".//label[@class='checkbox']")
    @FieldName(name = "Я хочу застраховать свой кредит")
    public WebElement checkboxInsurance;

    @FindBy(xpath = ".//span[text() = 'Получить кредит']")
    @FieldName(name = "Получить кредит")
    public WebElement buttonGetLoan;
}
