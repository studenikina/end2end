package ru.otpbank.testAnketa.page;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.settings.DriverManager;

import javax.swing.*;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractPageObject {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractPageObject.class);

    WebDriverWait wait = new WebDriverWait(DriverManager.getDriver(),15);
    public AbstractPageObject(){
        PageFactory.initElements(DriverManager.getDriver(),this);}

    /**
     * Метод прокрутки страницы до необходимого элемента
     * @param element - веб-элемент
     */
    public void scrollElemets (String element){
        ((JavascriptExecutor) DriverManager.getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    /**
     * Метод получения веб-элемента в DOM моделе сайта по его наименованию
     * @param name - наименование поля
     * @return - веб-элемент
     */
    public WebElement getField(String name) {
        Class clazz;
        try {
            clazz = Class.forName(this.getClass().getCanonicalName());
        } catch (ClassNotFoundException e) {
            LOG.error("При попытке получения поля класс не был найден");
            throw new NullPointerException("При попытке получения поля класс не был найден " + e);
        }

        List<Field> fields = Arrays.asList(clazz.getFields());
        for (Field field : fields){
            if (field.getAnnotation(FieldName.class).name().equals(name.trim())){
                if (!"".equals(field.getAnnotation(FindBy.class).css())) {
                    return DriverManager.getDriver().findElement(By.cssSelector(field.getAnnotation(FindBy.class).css()));
                }
                if (!"".equals(field.getAnnotation(FindBy.class).xpath())) {
                    return DriverManager.getDriver().findElement(By.xpath(field.getAnnotation(FindBy.class).xpath()));
                }
                if (!"".equals(field.getAnnotation(FindBy.class).id())){
                    return DriverManager.getDriver().findElement(By.id(field.getAnnotation(FindBy.class).id()));
                }
                if (!"".equals(field.getAnnotation(FindBy.class).linkText())){
                    return DriverManager.getDriver().findElement(By.linkText(field.getAnnotation(FindBy.class).linkText()));
                }
            }
        }

        throw new NullPointerException("Не объявлен элемент с наименованием " + name);
    }

    /**
     * Метод получения свойства класса по наименованию поля
     * @param name - наименование поля
     * @return - тестовое значение свойства поля
     * @throws Exception
     */
    public String getWebElement (String name) throws Exception {
        Class clazz = Class.forName(this.getClass().getCanonicalName());
        List<Field> fields = Arrays.asList(clazz.getFields());
        for (Field field : fields) {
            if (field.getAnnotation(FieldName.class).name().equals(name.trim())) {
                if (!"".equals(field.getAnnotation(FindBy.class).css())) {
                    return String.valueOf(field.getName());
                }
                if (!"".equals(field.getAnnotation(FindBy.class).xpath())) {
                    return String.valueOf(field.getName());
                }
                if (!"".equals(field.getAnnotation(FindBy.class).id())) {
                    return String.valueOf(field.getName());
                }
                if (!"".equals(field.getAnnotation(FindBy.class).linkText())){
                    return String.valueOf(field.getName());
                }
            }
        }
        Assert.fail("Не объявлен элемент с наименованием " + name);
        return null;
    }

    /**
     * Метод заполнения поля на сайте
     * @param name - название поля
     * @param value - значение, которым будет заполнено поле
     * @throws Exception
     */
    public void fillField(String name, String value) {
        WebElement element = getField(name);
        element.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), value);
    }

    /**
     * Метод получения теста ошибки при незаполненном поле
     * @param name - наименование поля
     * @return
     * @throws Exception
     */
    public String getErrorFromField(String name) {

        if (getField(name).findElement(By.xpath("ancestor::div[@class='field-container__content']/following-sibling::div")).isEnabled()){
            return getField(name).findElement(By.xpath("ancestor::div[@class='field-container__content']/following-sibling::div")).getText();
        }
        return null;
    }

    /**
     * Метод получения значения веб-элемента
     * @param name - веб-элемент
     * @return
     */
    public String getElementValue (String name) {
    return getField(name).getAttribute("value");}


    /**
     * Метод получения тестового значения для
     * @param name
     * @return
     */
    public String getElementText (String name) {
        return getField(name).getText();}


    /**
     * Метод получения текста внутри веб-элемента веб-элемента
     * @param name - веб-элемент
     * @return
     */
    public String getTextValueFromValue (String name) {
        return getField(name).getText();
    }

    /**
     * Метод выбора значения через выпадающий список
     * @param name
     * @param value -  значение строки в выпадаюшем списке
     * @throws Exception
     */
    public void choiseValueElem (String name,  String value){
        getField(name).click();
//        DriverManager.getDriver().findElement(By.xpath(".//div[aria-activedescendant ='react-select-2--option-3']")).getAttribute(value)
//        //new Select(getField(name)).selectByValue(value);
//        System.out.println(selectedOptions);
        DriverManager.getDriver().findElement(By.xpath(".//div[text() = '"+ value +"']")).click();

    }

    /**
     * Метод нажатия на элемент
     * @param element - наименование веб-элемента
     * @throws Exception
     */
    public void clickElemet (String element) {
        wait.until(ExpectedConditions.elementToBeClickable(getField(element))).click();
    }




}
