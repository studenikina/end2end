package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.annotation.PageName;

/**
 * Page Object, описывающий форму "Выбор даты и места получения кредитной заявки"
 */
@PageName(name = "Осталось выбрать способ получения кредита", path = ".//h2[@class='heading heading_size_2 heading_center withdrawal-method__title']")
public class DateAndAdress extends AbstractPageObject {

    @FindBy(css = "[name = 'delivery_date']")
    @FieldName(name = "Дата визита")
    public WebElement dateVisit;

    @FindBy(xpath = ".//span[text() = ' Списком']" )
    @FieldName(name = "Списком")
    public WebElement buttonList;

    @FindBy(css = "[placeholder = 'Адрес']")
    @FieldName(name = "Адрес точки")
    public WebElement address;

    @FindBy(xpath = ".//div[@class = 'rt-tbody']/div[1]")
    @FieldName(name = "Первый найденный адрес")
    public WebElement placeOfVisit;

    @FindBy(xpath = ".//div[@class='branch-map']")
    @FieldName(name = "Карта")
    public WebElement map;

    @FindBy(xpath= ".//div[@class = 'rt-tbody']//div[2]")
    @FieldName(name = "")
    public WebElement organizationType;

    @FindBy(xpath = "//label[text()='Куда доставить']/ancestor::div[1]/following-sibling::div[1]")
    @FieldName(name = "Куда доставить")
    public WebElement deliveryPlace;

    @FindBy(css= "[name = 'delivery_address']")
    @FieldName(name = "Адрес доставки")
    public WebElement deliveryAddress;

    @FindBy(xpath = "//label[text()='Комментарий']/ancestor::div[1]/following-sibling::div[1]")
    @FieldName(name = "Комментарий")
    public WebElement Комментарий;

    @FindBy(xpath = "//span[text()= 'Выбрать']")
    @FieldName(name = "Кнопка выбрать")
    public WebElement choiceButton;

    @FindBy(xpath = "//span[text()= 'Подтвердить']")
    @FieldName(name = "Кнопка Подтвердить")
    public WebElement confirmButton;

    @FindBy(css = "[class = 'courier-table']")
    @FieldName(name = "Дата для курьера")
    public WebElement courierCalendar;

    @FindBy(xpath = ".//h2[@class='heading heading_size_2 heading_center result__heading']")
    @FieldName(name = "Мы ждем вас в ")
    public WebElement placeWaiting;

    @FindBy(xpath = ".//div[text()='Адрес:']/following-sibling::div")
    @FieldName(name = "Адрес")
    public WebElement addressForUA;

    @FindBy(xpath = ".//div[text()='Дата посещения:']/following-sibling::div")
    @FieldName(name = "Дата посещения")
    public WebElement dateVizit;

    @FindBy(xpath = ".//div[@class='alert__content']")
    @FieldName(name = "Напоминание")
    public WebElement prompt;

}
