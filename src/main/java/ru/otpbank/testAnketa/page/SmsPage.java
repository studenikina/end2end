package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.annotation.PageName;

/**
 * Page Object, описывающий форму "СМС"
 */
@PageName(name = "На ваш телефон был отправлен", path = ".//div[@class='sms__subtitle']")
public class SmsPage extends AbstractPageObject {

    @FindBy(css = "[name = 'cell_phone_confirmation_code']")
    @FieldName(name = "Код из СМС")
    public WebElement codeSMS;

    @FindBy(xpath = ".//span[text() = 'Подтвердить']")
    @FieldName(name = "Подтвердить")
    public WebElement buttonBack;

    @FindBy(xpath = ".//div[@class = 'sms__subtitle']")
    @FieldName(name = "На ваш телефон")
    public WebElement introductoryText;

    @FindBy(xpath = ".//label[text() = 'Код из смс']/ancestor::div[@class = 'field-container']/descendant::div[@class ='field-error field-container__error']")
    @FieldName(name = "Тест ошибки")
    public WebElement errorText;



}
