package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.annotation.PageName;

/**
 * Page Object, описывающий форму "Ожидание решения по УА на скоринге"
 */
@PageName(name = "до онлайн-решения осталось", path = ".//div[@class='spinner result__spinner']")
public class ScoringWait extends AbstractPageObject {

    @FindBy(xpath = ".//span[text() = 'Перезагрузить страницу']")
    @FieldName(name = "Перезагрузить страницу")
    public WebElement buttonReboot;

    @FindBy(xpath = ".//h2[@class='heading heading_size_2 heading_center result__heading']")
    @FieldName(name = "Ожидание скоринга")
    public WebElement textWaitDecision;

    @FindBy(xpath = ".//h2[@class = 'heading heading_size_2 heading_center result__heading']/span")
    @FieldName(name = "Время ожидания")
    public WebElement timeOutWait;
}
