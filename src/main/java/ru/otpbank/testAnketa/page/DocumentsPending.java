package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.PageName;

/**
 * Page Object, описывающий форму "Прикрепление документов по УА"
 */
@PageName(name = "Прикрепление документов по УА", path = "")
public class DocumentsPending extends AbstractPageObject {

    public WebDriver driver;

    @FindBy(xpath = ".//div[@class = 'result']/descendant::h2[1]")
    WebElement docReviewUA;

}
