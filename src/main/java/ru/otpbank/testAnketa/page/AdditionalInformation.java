package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.annotation.PageName;

/**
 * Page Object, описывающий форму "Дополнительная информация"
 */
@PageName(name = "Дополнительная информация", path = ".//h3[text()= 'Дополнительная информация']")
public class AdditionalInformation extends AbstractPageObject {

    @FindBy(xpath = ".//label[text() = 'семейное положение']/parent::div/following-sibling::div[1]")
    @FieldName(name = "Семейное положение")
    public WebElement maritalStatus;

    @FindBy(css = "[name = 'number_of_children']")
    @FieldName(name = "Количество детей")
    public WebElement children;

    @FindBy(xpath = ".//label[text() = 'цель кредита']/parent::div/following-sibling::div[1]")
    @FieldName(name = "Цель кредита")
    public WebElement loanPurpose;

    @FindBy(css = "[name = 'contact_fio']")
    @FieldName(name = "Контактное лицо")
    public WebElement contactName;

    @FindBy(css = "[name = 'contact_phone']")
    @FieldName(name = "Телефон контактного лица")
    public WebElement contactPhone;

    @FindBy(css = "[name = 'code_word']")
    @FieldName(name = "Кодовое слово")
    public WebElement codeWord;

    @FindBy(css = "[name = 'school_number']")
    @FieldName(name = "Номер школы")
    public WebElement schoolNumber;

    @FindBy(css = "[name = 'mother_birthday']")
    @FieldName(name = "День рождения матери")
    public WebElement motherBirthday;

    @FindBy(xpath = ".//span[text() = 'У меня в собственности есть транспортное средство']")
    @FieldName(name = "Транспортное средство")
    public WebElement vehicle;

    @FindBy(xpath = ".//span[text() = 'У меня в собственности есть недвижимость']")
    @FieldName(name = "Недвижимость")
    public WebElement property;

    @FindBy(xpath = ".//span[text() = 'Я не являюсь ответчиком в судебном споре']")
    @FieldName(name = "Судебный иск")
    public WebElement legalAction;

    @FindBy(xpath = ".//span[text() = 'Узнать решение']")
    @FieldName(name = "Узнать решение")
    public WebElement buttonDecision;

    @FindBy(css = "[name = 'driver_license']")
    @FieldName(name = "Номер водительского удостоверения")
    public WebElement driversIdNumber;
}
