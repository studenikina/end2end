package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.annotation.PageName;

/**
 * Page Object, описывающий форму "Работа"
 */
@PageName(name = "Работа", path = ".//h3[text()='Работа']")
public class Job extends AbstractPageObject {

    @FindBy(xpath = ".//label[text() = 'образование']/parent::div/following-sibling::div[1]")
    @FieldName(name = "Образование")
    public WebElement education;

    @FindBy(xpath = ".//label[text() = 'тип занятости']/parent::div/following-sibling::div[1]")
    @FieldName(name = "Тип занятости")
    public WebElement typeWork;

    @FindBy(css = "[name = 'org_name']")
    @FieldName(name = "Организация")
    public WebElement organizationName;

    @FindBy(css = "[name = 'org_address']")
    @FieldName(name = "Адрес организации")
    public WebElement organizationAddress;

    @FindBy(css = "[name = 'org_inn']")
    @FieldName(name = "ИНН организации")
    public WebElement organizationINN;

    @FindBy(xpath = ".//label[text() = 'отрасль организации']/parent::div/following-sibling::div[1]")
    @FieldName(name = "Отрасль организации")
    public WebElement branchOfOrganization;

    @FindBy(css = "[name = 'work_phone1']")
    @FieldName(name = "Рабочий телефон")
    public WebElement workPhone;

    @FindBy(css = "[name = 'org_position']")
    @FieldName(name = "Должность")
    public WebElement position;

    @FindBy(css = "[name = 'experience_in_company']")
    @FieldName(name = "Стаж")
    public WebElement experience;

    @FindBy(css = "[name = 'income_main']")
    @FieldName(name = "Доход на основном месте")
    public WebElement income;

    @FindBy(css = "[name = 'expenses']")
    @FieldName(name = "Расходы")
    public WebElement costs;

    @FindBy(xpath = ".//span[text()='Далее'][@class = 'button__content']")
    @FieldName(name = "Далее")
    public WebElement buttonNext;
}
