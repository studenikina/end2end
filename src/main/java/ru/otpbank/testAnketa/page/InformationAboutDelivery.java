package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.annotation.PageName;

/**
 * Page Object, описывающий форму "Информация о доставке"
 */
@PageName(name = "Информация о доставке", path = "")
public class InformationAboutDelivery extends AbstractPageObject {

    @FindBy(xpath = "//div[text() = 'Доставка по адресу:']/following-sibling::div[1]")
    @FieldName(name = "Доставка по адресу")
    WebElement deliveryTo;

    @FindBy(xpath = "//div[text() = 'Дата доставки:']/following-sibling::div[1]")
    @FieldName(name = "Дата доставки")
    WebElement dateDelivery;

    @FindBy(xpath = "//div[text() = 'Время доставки:']/following-sibling::div[1]")
    @FieldName(name = "Время доставки")
    WebElement timeDelivery;

    @FindBy(css = "[class = 'alert__content']")
    @FieldName(name = "Напоминание")
    WebElement reminderInformation;


}
