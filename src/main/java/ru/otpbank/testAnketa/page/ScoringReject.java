package ru.otpbank.testAnketa.page;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.annotation.PageName;


@PageName(name = "Отказ скоринга", path = ".//div[@class='result__state-icon']")
public class ScoringReject {

    @FindBy(xpath = ".//h2[@class = 'heading heading_size_2 heading_center result__heading']")
    @FieldName(name = "Отказ скоринга")
    public WebElement scorRejectedUA;

    @FindBy(xpath = ".//h4[@class = 'heading heading_size_4 heading_center result__description']")
    @FieldName(name = "Рекомендация")
    public WebElement recommendation;
}
