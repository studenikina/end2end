package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Portal extends AbstractPageObject {
    public Portal(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    public WebDriver driver;

    @FindBy(xpath = ".//span[text() = 'Далее']")
    WebElement buttonNext;

    @FindBy(xpath = ".//span[text() = 'Назад']")
    WebElement buttonBack;

    @FindBy(xpath = ".//span[text() = 'Заказать карту']")
    WebElement buttonOrderCard;


}
