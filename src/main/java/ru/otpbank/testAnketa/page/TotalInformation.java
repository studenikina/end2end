package ru.otpbank.testAnketa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.otpbank.testAnketa.annotation.FieldName;
import ru.otpbank.testAnketa.annotation.PageName;

/**
 * Page Object, описывающий форму "Общие данные"
 */
@PageName(name = "Общие данные", path = ".//h3[text()= 'Общие данные']")
public class TotalInformation extends AbstractPageObject {

    @FindBy(css = "[name = 'monthly_income']")
    @FieldName(name = "Ваш ежемесячный доход")
    public WebElement monthlyIncome;

    @FindBy(css = "[name= 'payment_on_current_loans']")
    @FieldName(name = "Суммарный платеж по кредитам")
    public WebElement totalLoanPayment;

    @FindBy(css = "[name = 'requested_amount']")
    @FieldName(name = "Сумма кредита")
    public WebElement creditLoan;

    @FindBy(css= "[name = 'term']")
    @FieldName(name = "Срок кредитования")
    public WebElement loanTerms;

    @FindBy(css = "[name = 'last_name']")
    @FieldName(name = "Фамилия")
    public WebElement lastName;

    @FindBy(css = "[name = 'first_name']")
    @FieldName(name = "Имя")
    public WebElement firstName;

    @FindBy(css = "[name = 'middle_name']")
    @FieldName(name = "Отчество")
    public WebElement middleName;

    @FindBy(xpath = ".//input[@name = 'cell_phone']")
    @FieldName(name = "Мобильный телефон")
    public WebElement mobilePhone;

    @FindBy(css = "[name = 'email']")
    @FieldName(name = "eMail")
    public WebElement eMail;

    @FindBy(css = "[name = 'birth_date']")
    @FieldName(name = "Дата рождения")
    public WebElement birthDate;

    @FindBy(css = "[id = 'city_of_receiving_field']")
    @FieldName(name = "Город получения кредита")
    public WebElement loanReceiptCredit;

    @FindBy(xpath = ".//span[text()='Далее'][@class = 'button__content']")
    @FieldName(name = "Далее")
    public WebElement buttonNext;

    @FindBy(css =  "[name = 'agree_pers_data_flg']")
    @FieldName(name = "Согласие на обработку")
    public WebElement agreement;
}

