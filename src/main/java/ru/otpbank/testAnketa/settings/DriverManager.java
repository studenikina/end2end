package ru.otpbank.testAnketa.settings;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverManager {

    private static WebDriver driver = null;

    public static WebDriver getDriver() {
        if (driver == null) {
            driver = createDriver();
        }
        return driver;
    }

    public static void quiteDriver(){
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }

    private static WebDriver createDriver(){
        switch (TestProperties.getInstance().getProperties().getProperty("browser")) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", TestProperties.getInstance().getProperties().getProperty("webdriver.chrome.driver"));
                driver = new ChromeDriver();
                break;
            case "firefox":
                System.setProperty("webdriver.chrome.driver", TestProperties.getInstance().getProperties().getProperty("webdriver.gecko.driver"));
                driver = new ChromeDriver();
                break;
            default:
                driver = new ChromeDriver();
                break;
        }
        return driver;
    }
}
