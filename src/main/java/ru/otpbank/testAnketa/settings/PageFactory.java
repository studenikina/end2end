package ru.otpbank.testAnketa.settings;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.otpbank.testAnketa.annotation.PageName;
import ru.otpbank.testAnketa.page.AbstractPageObject;
import ru.otpbank.testAnketa.page.AdditionalInformation;

import java.util.HashMap;
import java.util.Map;

public class PageFactory {
    private static PageFactory pageFactory = null;

    private static AbstractPageObject currentPage;
    private static final Map<String, Class<? extends AbstractPageObject>> PAGES = new HashMap<>();
    private static final String PAGES_PACKAGE = "ru.otpbank.testAnketa.page"; // TODO: Change
    private static final Logger LOG = LoggerFactory.getLogger(PageFactory.class);

    static {
        Reflections reflections = new Reflections(PAGES_PACKAGE);
        for (Class<? extends AbstractPageObject> clazz : reflections.getSubTypesOf(AbstractPageObject.class)) {
            PageName pageName = clazz.getAnnotation(PageName.class);
            if (pageName == null) {
                LOG.error("Страница [" + clazz + "] не содержит аннотации @PageName. Данная страница будет недоступна при поиске.");
                continue;
            }

            PAGES.put(pageName.name(), clazz);
        }
    }

    public static PageFactory getPageFactory() {
        if (pageFactory == null) {
            pageFactory = new PageFactory();
        }

        return pageFactory;
    }

    public AbstractPageObject getPage(String name) {
        final Class<?extends AbstractPageObject> clazz = PAGES.get(name);
        if (clazz == null) {
            throw new NullPointerException("Страница \"" + name + "\" не найдена.");
        }

        try {
            AbstractPageObject page = clazz.newInstance();
            setCurrentPage(page);
            return page;
        } catch (InstantiationException e) {
            LOG.error("Что-то пошло не так про попытке создать страницу. " + e);
            throw new NullPointerException("При создании страницы произошла ошибка!");
        } catch (IllegalAccessException e) {
            LOG.error("Что-то пошло не так про попытке создать страницу. " + e);
            throw new NullPointerException("При создании страницы произошла ошибка!");
        }
    }

    public AbstractPageObject getCurrentPage() {
        return currentPage;
    }

    private static void setCurrentPage(AbstractPageObject page) {
        currentPage = page;
    }
}
